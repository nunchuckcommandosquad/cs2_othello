#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <tuple>

using namespace std;

class Player {
private:
    
    Side s;
    vector<Move> ValidMoves(Board board, Side sde);
    //int getScore(Board board, Side curSide);
    int getScore(Board board, Move *move, Side sde);
    void MiniMax(Board board, int max_depth, int depth, Side side, Move *chosenmove, int &chosenscore);
    //double getScore(Move *move);
    //Move *m;
public:
    Player(Side side);
    ~Player();
    Board b;
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
