Question 1:
For the past two weeks we would work together to write our AI. We would come to Annenburg and work. We wrote the code together so even when one person was writing the code the other person was there and involved. We would brainstorm ideas to improve our heuristic. We pretty miuch did everything together so both of us were there when we were improving our AI. 

Question 2:
To make our AI tournament worthy we made a matrix of weights in which we gave high weights to moves that would result in our pices being on the corneer or edges and low weights to moves that would give the opponent an opportunity to get their pieces into the corner or edges. We also increased the weight of a move that would result in a full row or column of our color. Furthermore we increased the weight of a move that limited the mobility of the oppponent. Lastly, if a move could result in a major diagonal containing only our pieces then we also weighted that move heavily. 
We wrote the code for minimax and it worked when we tested it, however implementing minimax actuallu made our AI peform worse than just using our heuristic so we decided not to use it. 
