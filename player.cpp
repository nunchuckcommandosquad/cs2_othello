#include "player.h"
#include <vector>
#include <stdlib.h>
#include <limits>
#include <tuple>

const double MULT_CORNER = 100; //20 //4 //3
const double MULT_EDGE = 25;  //15 //5
const double MULT_INNERCORNER = -25; //-10 //0.25 // -3
const double MULT_BADEDGE = -10; //10 //0.5 // -2
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
	/* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    s = side; 					//0 is WHITE, 1 is BLACK;
    Board b = Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {

}

/*
 * 
 * Finds all valid moves on board and returns them as a vector of moves.
 * 
 */
std::vector<Move> Player::ValidMoves(Board board, Side sde)
{
    std::vector<Move> Valid;
    
    for (int r = 0; r < 8; r++)
    {
        for (int c = 0; c < 8; c++)
        {
            Move move = Move(r, c);
            if (board.checkMove(&move, sde) == true)
            {
                Valid.push_back(move);
            }
        }
    }
    return Valid;   
}

int Player::getScore(Board board, Move *move, Side sde)
{
    int x, y;
    int weight[8][8];
    int occ, score;
    int our_side = 0;
    int opp = 0;
    bool diag1 = true;
    bool diag2 = true;
    Side opponent = (Side) ((sde + 1) % 2);
    
    Board bcopy = *board.copy();
	bcopy.doMove(move, sde);
	Side occby;
    
    if (testingMinimax == true)
    {
		//Side opponent = (Side) ((sde + 1) % 2);
        score = bcopy.count(sde) - bcopy.count(opponent);
        return score;
    }
    
    for (x = 0; x < 8; x++)
    {
        for (y = 0; y <8; y++)
        {
            if ((x == 0 && y == 0) || (x == 7 && y == 7) || (x == 0 && y == 7) || (x == 7 && y == 0))
            {
                weight[x][y] = MULT_CORNER;
            }
            else if ((x == 0 && y == 1) || (x == 1 && y == 0) || (x == 6 && y == 0) || (x == 7 && y == 1) ||
	                (x == 0 && y == 6) || (x == 1 && y == 7) || (x == 7 && y == 6) || (x == 6 && y == 7))
	        {
	            weight[x][y] = MULT_BADEDGE;
	        }
	        else if (x == 0 || x == 7 || y == 0 || y == 7)
	        {
	            weight[x][y] = MULT_EDGE;
	        }
	        else if ((x == 1 && y == 1) || (x == 6 && y == 1) || (x == 1 && y == 6) || (x == 6 && y ==6))
	        {
	            weight[x][y] = MULT_INNERCORNER;
	        }
	        else
	        {
	            weight[x][y] = 1;
	        }
	     }
	  }
	  
	  
	  for (x = 0; x < 8; x++)
	  {
	      for (y = 0; y < 8; y++)
	      {
	          occ = bcopy.occupiedBy(x, y);
	          if (occ != -1)
	          {
	            occby = (Side) (occ);
	            if (occby == sde)
	            {
	                our_side += weight[x][y];
	            }
	            else
	            {
	                opp += weight[x][y];
	            }
	           }    
	       }
	   }
	   
	   score = our_side - opp;
	   
	   
	   bool onecolorRow = true;
	   bool onecolorCol = true;
	   
	   for (int i = 0; i < 8; i++)
	   {
		   occ = bcopy.occupiedBy(i, move->getY());
		   if (occ >= 0)
		   {
			   if (((Side) occ) != sde)
			   {
				   onecolorCol = false;
			   }
		   }
	   }
	   
	   for (int i = 0; i < 8; i++)
	   {
		   occ = bcopy.occupiedBy(move->getX(), i);
		   if (occ >= 0)
		   {
			   if (occ >= 0)
			   {
				   if (((Side) occ) != sde)
				   {
					   onecolorRow = false;
				   }
			   }
		   }
	   }
	   
	   if (onecolorRow == true) {
		   score += 75;
	   }
	   
	   if (onecolorCol == true) {
			score += 75;
	   }
	   
	   for (int row = 0; row < 8; row++)
	   {
	   occ = bcopy.occupiedBy(row, row);
	       if (occ >= 0)
	       {
	           if (((Side) occ) != sde)
	           {
	               diag1 = false;
	           }
	       }
	   }
	   
	   int j = 8;
	   for (int row = 0; row < 8; row++)
	   {
	       j--;
	       occ = bcopy.occupiedBy(row, j);
	       if (occ >= 0)
	       {
	           if (((Side) occ) != sde)
	           {
	               diag2 = false;
	           }
	       }
	   }
	   
	   if (diag1 == true || diag2 == true)
	   {
	       score += 50;
	   }
	                 
	   score -= (ValidMoves(bcopy, opponent).size())* 15;
	   if (ValidMoves(bcopy, opponent).size() == 0)
	   {
	        score += 200;
	   }
	   return score;
}

      
void Player::MiniMax(Board board, int max_depth, int depth, Side side, 
Move *chosenmove, int &chosenscore)
{
	if (depth == max_depth)
	{
		chosenscore = board.count(s) - board.count((Side)((s+1) % 2));
		return;
	}
	
	int bestscore;
	Move *bestMove = new Move(-1, -1);
	
	std::vector<Move> possibleMoves = ValidMoves(board, side);
	
	if (possibleMoves.size() == 0)
	{
		chosenscore = board.count(s) - board.count((Side)((s+1) % 2));
		return;
	}
	
	if (side == s)
	{
		bestscore = std::numeric_limits<int>::min();
	}
	else
	{
		bestscore = std::numeric_limits<int>::max();
	}
	
	for (int i = 0; i < possibleMoves.size(); i++)
	{
		Board copy = *board.copy();
		copy.doMove(&possibleMoves[i], side);
		Move *tempmove = new Move(possibleMoves[i].getX(), possibleMoves[i].getY());
		int tempscore = 0;
		
		MiniMax(copy, max_depth, depth+1, (Side)((side+1)%2), tempmove, tempscore);
		
		if (side == s)
		{
			if (tempscore > bestscore)
			{
				bestscore = tempscore;
				bestMove->setX(possibleMoves[i].getX());
				bestMove->setY(possibleMoves[i].getY());
			}
		}
		else
		{
			if (tempscore < bestscore)
			{
				bestscore = tempscore;
				bestMove->setX(possibleMoves[i].getX());
				bestMove->setY(possibleMoves[i].getY());
			}
		}
		
		chosenmove->setX(bestMove->getX());
		chosenmove->setY(bestMove->getY());
		chosenscore = bestscore;
		return;	//fix construct tuple
	}
}


    
/*  
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     Side opp = (Side) ((s + 1) % 2);
     b.doMove(opponentsMove, opp);
     
     if(b.hasMoves(s) == false)
     {
        return NULL;
     }
     else if (testingMinimax == true) {
		int score;
		Move *mov = new Move(-1, -1);
		MiniMax(b, 2, 0, s, mov, score);
		b.doMove(mov, s);
		return mov;
	 }
     else
     {
		int max_score;

		int index_bestMove;
        std::vector<Move> valid = ValidMoves(b, s);
        
        max_score = getScore(b, &valid[0], s);
        index_bestMove = 0;
        for (unsigned int i = 1; i < valid.size(); i++)
        {
			if (getScore(b, &valid[i], s) > max_score)
			{
				max_score = getScore(b, &valid[i], s);
				index_bestMove = i;
			}
        }
        
        Move *m = new Move(valid[index_bestMove].getX(), valid[index_bestMove].getY());
        
        b.doMove(m, s);

        return m;
     }
}
